#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # -------------
    # 3 Read Tests
    # -------------
    def test_read_1(self):
        s = "1000000 1000000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1000000)
        self.assertEqual(j,  1000000)

    def test_read_2(self):
        s = "90 50\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  90)
        self.assertEqual(j,  50)

    def test_read_3(self):
        s = "2 7\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2)
        self.assertEqual(j,  7)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # ----
    # 3 Eval Tests
    # ----

    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(950, 600)
        self.assertEqual(v, 179)

    def test_eval_7(self):
        v = collatz_eval(5000, 3000)
        self.assertEqual(v, 238)
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # ---------------
    # 3 Print Tests
    # ---------------
    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 5, 6, 7)
        self.assertEqual(w.getvalue(), "5 6 7\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1000000, 1000000, 1000000)
        self.assertEqual(w.getvalue(), "1000000 1000000 1000000\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 90, 70, 50)
        self.assertEqual(w.getvalue(), "90 70 50\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # --------------
    # 3 Solve Tests
    # --------------
    def test_solve_2(self):
        r = StringIO("570 980\n57 98\n200 300\n20 30\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "570 980 179\n57 98 119\n200 300 128\n20 30 112\n")

    def test_solve_3(self):
        r = StringIO("8 9\n80 90\n800 900\n435 21\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "8 9 20\n80 90 111\n800 900 179\n435 21 144\n")

    def test_solve_4(self):
        r = StringIO("4 71\n92 20\n737 260\n790 10\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "4 71 113\n92 20 116\n737 260 171\n790 10 171\n")



# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage-3.5 run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage-3.5 report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
